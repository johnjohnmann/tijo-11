#Fabryka abstrakcyjna (wzorzec projektowy)
Fabryka abstrakcyjna (ang. *Abstract Factory*) – kreacyjny wzorzec projektowy, którego celem jest dostarczenie interfejsu do tworzenia różnych obiektów jednego typu (tej samej rodziny) bez specyfikowania ich konkretnych klas. Umożliwia jednemu obiektowi tworzenie różnych, powiązanych ze sobą, reprezentacji podobiektów określając ich typy podczas działania programu. *Fabryka abstrakcyjna* różni się od *Budowniczego* tym, że kładzie nacisk na tworzenie produktów z konkretnej rodziny, a *Budowniczy* kładzie nacisk na sposób tworzenia obiektów.

##Przykład zastosowania
Rozpatrzmy aplikację kliencką, która łączy się ze zdalnym serwerem. Celem projektanta takiej aplikacji jest to, aby była ona przenośna. Jednym z rozwiązań takiego problemu jest stworzenie fabryki, która będzie tworzyła odpowiednie obiekty w zależności od tego na jakiej platformie się znajduje.

##Struktura wzorca
Jak widać na załączonym diagramie klas wzorzec zbudowany jest z kilku podstawowych klas. Klasa *Fabryka abstrakcyjna* deklaruje abstrakcyjny interfejs umożliwiający tworzenie produktów. Interfejs ten jest implementowany w *Fabrykach konkretnych*, które odpowiedzialne są za tworzenie konkretnych produktów. Każda fabryka konkretnego produktu posiada także metodę wytwórczą tego produktu.

![Diagram klas](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Abstract_factory_UML.svg/1024px-Abstract_factory_UML.svg.png)

##Konsekwencje
Jednym z plusów wykorzystania wzorca jest możliwość ukrycia szczegółów implementacyjnych klas reprezentujących konkretny produkt - klient widzi tylko interfejs. Ukryciu ulegają także nazwy tych klas, co nie wymusza ich zapamiętywania i odizolowuje klienta od problemu określenia do której klasy należy obiekt.

Do zysków należy także możliwość całkowitego ukrycia implementacji obiektów przed klientem. Klient widzi tylko interfejs i nie ma możliwości zajrzenia do kodu oraz to, że wymuszana jest spójność produktów.

Do minusów należy zaliczyć trudność rozszerzania rodziny obiektów o nowe podobiekty. Wymusza to modyfikację klasy fabryki abstrakcyjnej oraz wszystkich obiektów, które są tworzone przez nią.

Źródło - [Wikipedia](https://pl.wikipedia.org/wiki/Fabryka_abstrakcyjna_(wzorzec_projektowy))